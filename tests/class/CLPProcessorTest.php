<?php

namespace EPAM\training\PHP\Sample\Calculator;

use PHPUnit\Framework\TestCase;

spl_autoload_register(function ($class) {

    $prefix = "EPAM\\training\\PHP\\Sample\\Calculator\\";
    $base_dir = __DIR__ . '/../../src/class/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = str_replace('\\', '/', $base_dir . $relative_class) . '.php';

    echo $file;

    if (file_exists($file)) {
        require $file;
    }
});

class CLPProcessorTest extends TestCase
{

    public function testGetFirstOperand()
    {
        $clp = new CLPProcessor(array('', '3', '+', '2'));
        $this->assertEquals(3, $clp->getFirstOperand());
    }

    /**
     * @dataProvider getSecondOperandProvider
     */
    public function testGetSecondOperand(string $clp1, string $clp2, string $clp3, string $clp4)
    {
        $clp = new CLPProcessor(array($clp1, $clp2, $clp3, $clp4));
        $this->assertEquals($clp4, $clp->getSecondOperand());
    }

    public function getSecondOperandProvider(): array
    {
        return [
            ['', '2', '+', '3'],
            ['', '1', '-', '4.56'],
            ['', '34', '*', '999'],
            ['', '16', '/', '8']
        ];
    }

}
