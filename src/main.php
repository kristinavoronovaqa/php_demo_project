<?php

namespace EPAM\training\PHP\Sample\Calculator;

spl_autoload_register(function ($class) {

    $prefix = "EPAM\\training\\PHP\\Sample\\Calculator\\";
    $base_dir = __DIR__ . '/class/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = str_replace('\\', '/', $base_dir . $relative_class) . '.php';

    if (file_exists($file)) {
        require $file;
    }
});

$clp = new CLPProcessor($argv);
$calc = new Calc(5);

switch ($clp->getOperation()) {
    case "+":
        echo $calc->sum($clp->getFirstOperand(), $clp->getSecondOperand());
        break;
    case "-":
        echo $calc->sub($clp->getFirstOperand(), $clp->getSecondOperand());
        break;
    case "*":
        echo $calc->mul($clp->getFirstOperand(), $clp->getSecondOperand());
        break;
    case "/":
        echo $calc->div($clp->getFirstOperand(), $clp->getSecondOperand());
        break;
}